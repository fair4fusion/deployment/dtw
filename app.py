import logging
import os
import sys
from flask import Flask, request
from flask_restful import Api
from dtaidistance import dtw
import requests
import numpy as np
import threading


app = Flask(__name__)

app.config.update({
    'TESTING': True
})

api = Api(app)


@app.route('/outputs', methods=['POST'])
def outputs():
    id = request.form['id']
    fout = "/work/out/" + id
    if not os.path.exists(fout+"/status.file"):
        return {"status": "No workflow with this id"}
    else:
        with open(fout+"/status.file", 'r') as file:
            status = file.read()
            if not status == "Succeeded":
                return {"status": status}
    output_json = {"most_similar": [], "least_similar": []}
    with open(os.path.join(fout, "output_smallest.txt"), 'r') as f:
        content = f.readlines()
        for line in content:
            for x in line.split():
                output_json["most_similar"].append(int(x))
    with open(os.path.join(fout, "output_largest.txt"), 'r') as f:
        content = f.readlines()
        for line in content:
            for x in line.split():
                output_json["least_similar"].append(int(x))
    return output_json


@app.route('/status', methods=['POST'])
def status():
    id = request.form['id']
    fout = "/work/out/" + id
    if not os.path.exists(fout+"/status.file"):
        return {"status": "No workflow with this id"}
    else:
        with open(fout+"/status.file", 'r') as file:
            status = file.read()
            return {"status": status}


@app.route('/run', methods=['POST'])
def run():
    def task(**kwargs):
        run_id = kwargs.get('id')
        first_val = int(kwargs.get('first_val'))
        fn = "/data/"
        if not os.path.exists(fn):
            os.makedirs(fn)
        f_inter_out = "/work/inter_out/" + run_id
        if not os.path.exists(f_inter_out):
            os.makedirs(f_inter_out)
        fout = "/work/out/" + run_id
        if not os.path.exists(fout):
            os.makedirs(fout)
        with open(f'{fout}/status.file', 'w') as file:
            file.write("Submitted")
            file.close
        y = 0
        s = []
        idd = []
        for filename in os.listdir(fn):
            if filename.endswith(".b0_mean"):
                # logging.error(filename)
                (file, ext) = os.path.splitext(filename)
                idd.append(int(file))
                with open(os.path.join(fn, filename), 'r') as f:
                    s1 = np.fromfile(f, dtype=float, count=-1, sep=' ')
                    s1.reshape(-1, 2)
                    s.append(s1)

        # print(idd)
        print(first_val)
        pos = 0
        if first_val in idd:
            pos = idd.index(first_val)
        # logging.error(pos)

        for i in range(len(s)):
            if y != pos:
                distance, paths = dtw.warping_paths(s[pos], s[y])
                with open(f'{f_inter_out}/{idd[y]}.distance', 'w') as file:
                    file.write(str(distance))
                    file.close
            y = i+1

        payload = {'id': run_id, 'number_of_sim': '5'}
        with open(f'{fout}/status.file', 'w') as file:
            file.write("Passed first step")
            file.close
        r = requests.post(
            "http://similarity.fair4fusion.iit.demokritos.gr/run",  data=payload)
        if not r.ok:
            logging.error(r.content)
            return r.content

    # uuid_str = str(uuid.uuid4())
    if 'pulse_id' not in request.form:
        return {'error': 'No pulse id provided'}
    fout = "/work/out/" + request.form['pulse_id']
    if not os.path.exists(fout+"/status.file"):
        thread = threading.Thread(target=task, kwargs={
            'id': request.form['pulse_id'], 'first_val': request.form['pulse_id']})
        thread.start()
    return {"id": request.form['pulse_id']}


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

if __name__ == '__main__':
    app.run(debug=True)
