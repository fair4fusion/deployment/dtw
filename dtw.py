from dtaidistance import dtw
from dtaidistance import dtw_visualisation as dtwvis
import json
import numpy as np
import os
import glob
import sys


fn = sys.argv[1]
path = os.path.basename(fn)
fout = sys.argv[2]
path_out = os.path.basename(fout)

y = 0
s = []

for filename in glob.glob(os.path.join(path, '*.txt')):
    with open(os.path.join(os.getcwd(), filename), 'r') as f:
        for line in f:
            inner_list = [int(elt.strip()) for elt in line.split(',')]
            s.append(inner_list)
print(s)
for i in range(len(s)-1):
    y = i+1
    d = dtw.distance(s[0], s[y])
    # print("D",d)
    path = dtw.warping_path(s[0], s[y])
    dtwvis.plot_warping(
        s[0], s[y], path, filename=f"{path_out}/warp_{0}-{y}.png")

    distance, paths = dtw.warping_paths(s[0], s[y])
    print("DISTANCE", distance)
    print("PATHS", paths)

    with open(f'{path_out}/distance_{0}-{y}.txt', 'w') as file:
        file.write(str(distance))
        file.close

# # s1 = [0, 0, 1, 2, 1, 0, 1, 0, 0]
# # s[y] = [0, 1, 2, 0, 0, 0, 0, 0, 0]
